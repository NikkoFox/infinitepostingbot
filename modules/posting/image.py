import configparser
import glob
import logging
import os
import random
import re
from collections import defaultdict
from io import BytesIO

from PIL import Image

from api import vk_api
from api.yadisk_api import YADiskApi, download_image
from modules.anime_source import get_image_source
from modules.anime_title import parsing_image_name, find_title_in_db

Image.MAX_IMAGE_PIXELS = None
config = configparser.ConfigParser()
config.read('config.ini')


class ImagePoster:
    def __init__(self, count, post_type):
        self.vk_token = config['VK'].get("token")
        self.group_id = config['VK'].get("group_id")
        self.group_name = config['VK'].get("group_name")
        self.type = post_type
        self.image = ''
        self.name = ''
        self.message = ''
        self.source = ''
        self.yandex_del_path = ''
        path = config['BotSettings'].get('path')
        if self.type == "gif" and count == 1:
            all_img = glob.glob(path + r'/*.gif')
        elif self.type == "yandex" and count == 1:
            ya_token = config['Yandex'].get('token')
            self.yandex_api = YADiskApi(ya_token)
            _image, _name, _ = self.yandex_api.get_image()
            all_img = [{'image': _image, 'name': _name}]
        else:
            all_img = glob.glob(path + r'/*')

        if not all_img:
            logging.error("not image")
            exit()

        image = []
        if count > 1:
            message = f'#arts@{self.group_name} #anime #аниме'
            same_arts = search_img_same_title(all_img)
            img_count = random.randint(2, 10)
            logging.info('random count: %s', img_count)
            title_keys = list(same_arts.keys())
            random.shuffle(title_keys)
            while not image and img_count > 1:
                for title in title_keys:
                    logging.info('title: %s, count: %s', title, len(same_arts[title]))
                    if len(same_arts[title]) >= img_count:
                        image = same_arts[title][:img_count + 1]
                        break
                img_count -= 1
        else:
            message = f'#art@{self.group_name} #anime #аниме'
            image = [random.choice(all_img)]
        if not image:
            logging.error(f"not image")
            exit()
        self.message = message
        logging.info(f"POST_TYPE: {post_type}")
        if post_type == 'yandex' and count == 1:
            logging.info(image[0].get("name"))
            self.image = download_image(image[0].get("image"))
            self.name = image[0].get("name")
            self.source = get_image_source(self.name)
        else:
            logging.info(image)
            self.image = image
            self.name = image[0]
            self.source = get_image_source(image[0])

    def post(self):
        origin_name = parsing_image_name(os.path.split(self.name)[-1]).replace('  ', ' ')
        logging.info(f"ORIGIN_NAME: {origin_name}")
        if origin_name:
            title = origin_name.replace('_(series)', '').replace('_', ' ').replace(' (series)', ' ')
            title = re.sub(r':.*', '', title)
            title = find_title_in_db(title)
            if title:
                regexp = r"['\-,;.\s]"
                title = title.lower().replace('@', 'a').replace('!', '').replace('?', '')
                self.message += f"\n#{re.sub(regexp, '_', title)}@{self.group_name}"
        try:
            if self.type == "yandex":
                attachments = self.attachments_from_yandex_disk()
            else:
                attachments = self.attachments_from_disk()
        except Exception as e:
            logging.exception(e)
            return False
        self.message += f'\n\n{origin_name}' if '.gif' not in origin_name else ''
        # self.message += f"\nSource: {self.source}" if self.source else ""
        posting = vk_api.Posting(self.vk_token, self.group_id, message=self.message, attachments=attachments)
        try:
            posting.create_post(self.source if self.source else None)
        except Exception as e:
            logging.exception(e)
            return False
        if self.type == 'yandex':
            self.yandex_api.delete_image()
        else:
            for i in self.image:
                os.remove(i)
        return True

    def attachments_from_disk(self):
        attachments = []
        hq = ''
        for i in self.image:
            with Image.open(i) as img:
                if img.format == "GIF" and img.is_animated:
                    attachments.append(vk_api.Attachment(i, i, 'doc'))
                    self.message = self.message.replace(f"#art@{self.group_name}", f"#gif@{self.group_name}")
                else:
                    attachments.append(vk_api.Attachment(i, i, 'image'))
                    # if sum(img.size) >= 4000:
                    #     attachments.append(vk_api.Attachment(i, i, 'doc'))
                    #     hq = f"#hq@{self.group_name} "
        self.message = f"{hq}{self.message}"
        return attachments

    def attachments_from_yandex_disk(self):
        attachments = []
        with Image.open(BytesIO(self.image)) as img:
            if img.format == "GIF" and img.is_animated:
                attachments.append(vk_api.Attachment(self.name, self.image, 'doc'))
                self.message = self.message.replace(f"#art@{self.group_name}", f"#gif@{self.group_name}")
            else:
                attachments.append(vk_api.Attachment(self.name, self.image, 'image'))
                # if sum(img.size) >= 4000:
                #     attachments.append(vk_api.Attachment(self.name, self.image, 'doc'))
                #     self.message = f"#hq@{self.group_name} {self.message}"
        return attachments


def search_img_same_title(all_img):
    arts = defaultdict(list)
    for image_name in all_img:
        image_path = image_name
        image_name = os.path.split(image_name)[-1]
        anime_title = parsing_image_name(image_name)
        if anime_title:
            if os.path.splitext(image_name)[1].lower() in ['.png', '.jpg', '.jpeg']:
                arts[anime_title.lower()].append(image_path)
            else:
                continue
        else:
            continue
    return arts
