import re
from os import path


def get_image_source(image):
    source = ''
    if isinstance(image, list):
        temp = []
        for i in image:
            i = path.split(i)[-1]
            temp.append(get_image_source(i))
        return ','.join(temp)
    else:
        image = path.split(image)[-1]

    image_id = re.search(r"[-#]\s*(?P<number>\d{4,})", image)
    if image_id:
        image_id = image_id.group('number')
        if 'ZC ' in image or 'zerochan' in image:
            source = f'https://www.zerochan.net/{image_id}'
        elif 'e-shuushuu' in image:
            source = f'https://e-shuushuu.net/image/{image_id}/'
        elif 'chan.sankakucomplex' in image:
            source = f'https://chan.sankakucomplex.com/post/show/{image_id}'
        elif 'safebooru' in image:
            source = f'https://safebooru.org/index.php?page=post&s=view&id={image_id}'
        elif 'konachan' in image:
            source = f'https://konachan.net/post/show/{image_id}'
        elif 'gelbooru' in image:
            source = f'https://gelbooru.com/index.php?page=post&s=view&id={image_id}'
        elif 'yande' in image:
            source = f'https://yande.re/post/show/{image_id}'
        elif 'anime-pictures' in image:
            source = f'https://anime-pictures.net/pictures/view_post/{image_id}'
    return source
